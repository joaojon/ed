/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayListPackage;

import java.util.Iterator;
import java.util.ConcurrentModificationException;

import Exceptions.*;

/**
 * @author Joao
 */
public abstract class ArrayList<T> implements ListADT<T> {

    /**
     * Number to multiply by the size if we get the array full
     */
    private static final int EXPAND_BY = 2;
    /**
     * Default size of the array
     */
    private static final int DEFAULT_CAPACITY = 20;

    /**
     * array of objects
     */
    protected T[] list;
    /**
     * Next free position and number of elements in the list
     */
    protected int rear;
    /**
     * number of modifications
     */
    protected int modCount;

    /**
     * Constructor method for an array list with the default size of 20
     */
    public ArrayList() {
        this.list = (T[]) new Object[DEFAULT_CAPACITY];
        this.rear = 0;
        this.modCount = 0;
    }

    /**
     * Constructor method for an array list with the size of the given parameter
     *
     * @param arraySize initial size of the list
     */
    public ArrayList(int arraySize) {
        this.list = (T[]) new Object[arraySize];
        this.rear = 0;
        this.modCount = 0;
    }

    /**
     * Removes the first element of the list
     *
     * @return removed element
     * @throws EmptyCollectionException if the collection is empty
     */
    @Override
    public T removeFirst() throws EmptyCollectionException {
        if (size() == 0) {
            throw new EmptyCollectionException("A coleção encontra-se vazia");
        }

        T removed = this.list[0];
        for (int i = 0; i < this.rear - 1; i++) {
            this.list[i] = this.list[i + 1];
        }

        this.rear--;
        this.list[this.rear] = null;
        this.modCount++;

        return removed;
    }

    /**
     * Removes the last element of the list
     *
     * @return removed element
     * @throws EmptyCollectionException if the collection is empty
     */
    @Override
    public T removeLast() throws EmptyCollectionException {
        if (size() == 0) {
            throw new EmptyCollectionException("A coleção encontra-se vazia");
        }

        this.rear--;

        T removed = this.list[rear];

        this.list[this.rear] = null;
        this.modCount++;

        return removed;
    }

    /**
     * Method to remove a specific element of the list (given by parameter)
     *
     * @param element element to remove
     * @return removed element
     * @throws EmptyCollectionException if the collection is empty
     * @throws ElementNotFoundException if the element isn't found
     */
    @Override
    public T remove(T element) throws EmptyCollectionException, ElementNotFoundException {
        if (isEmpty()) {
            throw new EmptyCollectionException("A coleção encontra-se vazia");
        }

        boolean found = false;
        int i = 0;
        while (!found && i < rear) {
            if (this.list[i].equals(element)) {
                found = true;
            } else {
                i++;
            }
        }

        if (!found) {
            throw new ElementNotFoundException("O elemento que pretende remover não se encontra na lista. ");
        }

        T removed = this.list[i];

        for (int j = i; j < this.rear - 1; j++) {
            this.list[j] = this.list[j + 1];
        }

        this.list[this.rear - 1] = null;
        this.rear--;
        this.modCount++;

        return removed;
    }

    /**
     * Method that returns the first element of the list
     *
     * @return first element of the list
     * @throws EmptyCollectionException if the collection is empty
     */
    @Override
    public T first() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("A coleção encontra-se vazia");
        }

        return this.list[0];
    }

    /**
     * Method that return the last element of the list
     *
     * @return last element of the list
     * @throws EmptyCollectionException if the collection is empty
     */
    @Override
    public T last() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("A coleção encontra-se vazia");
        }

        return this.list[this.rear - 1];
    }

    /**
     * Method that verifies if a given element is in the list
     *
     * @param target element to verify
     * @return true if the element is in the list
     * @throws EmptyCollectionException if the collection is empty
     */
    @Override
    public boolean contains(T target) throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("A coleção encontra-se vazia");
        }

        boolean found = false;
        int i = 0;
        while (!found && i < rear) {
            if (this.list[i].equals(target)) {
                found = true;
            } else {
                i++;
            }
        }

        return found;
    }

    /**
     * Returns true if the list is empty
     *
     * @return true if the list is empty false otherwise
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns the size of the list
     *
     * @return the size of the list
     */
    @Override
    public int size() {
        return this.rear;
    }

    /**
     * Method that returns the iterator fo this list
     *
     * @return iterator for this list
     */
    @Override
    public Iterator<T> iterator() {
        return new arrayListIterator();
    }

    /**
     * Method that expands the actual capacity of the list by doubling it
     */
    protected void expandCapacity() {
        int newSize = (this.list.length * EXPAND_BY);
        T[] newList = (T[]) (new Object[newSize]);

        for (int i = 0; i < this.size(); i++) {
            newList[i] = this.list[i];
        }

        this.list = newList;
    }

    /**
     * Method that uses the iterator to create a string with all the infos of the non null elements of the list
     *
     * @return string with the infos of the non null elements of the list
     */
    @Override
    public String toString() {
        arrayListIterator i = (arrayListIterator) iterator();
        String r = "";
        int count = 0;

        try {
            while (i.hasNext()) {
                T element = (T) i.next();
                r += "Elemento na posição [" + (count++) + "]: " + (element.toString()) + "\n";
            }
        } catch (ConcurrentModificationException ex) {
            System.out.println(ex.getMessage());
        }

        r += "Tamanho do array: " + this.list.length + "\n";
        r += "Número de elementos: " + this.size();

        return r;
    }

    private class arrayListIterator implements Iterator {

        /**
         * Modifications counter
         */
        private int expectedModCount;
        /**
         * Position of the current element
         */
        private int current;
        /**
         * boolean value that represents if it's ok to remove
         */
        private boolean okToRemove;

        /**
         * Constructor method
         */
        public arrayListIterator() {
            this.current = 0;
            this.expectedModCount = modCount;
            this.okToRemove = false;
        }

        /**
         * Returns true if the next element isn't null false otherwise
         *
         * @return true if the next element isn't null false otherwise
         */
        @Override
        public boolean hasNext() {
            if (this.expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }

            this.okToRemove = false;

            return this.current != size();
        }

        /**
         * Returns the current element and updates it to the next one
         *
         * @return current element
         */
        @Override
        public T next() {
            if (!hasNext()) {
                throw new RuntimeException("O iterador já percorreu a coleção toda");
            }

            if (this.expectedModCount != modCount) {
                throw new ConcurrentModificationException();
            }

            T data = (T) list[this.current];
            this.current++;
            this.okToRemove = true;

            return data;
        }

        /**
         * Removes if possible the current element
         */
        @Override
        public void remove() {
            if (this.expectedModCount != modCount) {
                throw new ConcurrentModificationException("Já houveram modificações");
            }

            if (!this.okToRemove) {
                throw new ConcurrentModificationException("não é possível remover");
            }

            try {
                System.out.println("Elemento removido: " + ArrayList.this.remove(list[current - 1]));
            } catch (ElementNotFoundException | EmptyCollectionException ex) {
                System.out.println(ex.getMessage());
            }

            this.current--;
            this.expectedModCount = modCount;
            this.okToRemove = false;
        }
    }
}
