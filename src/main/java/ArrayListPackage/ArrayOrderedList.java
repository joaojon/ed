/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayListPackage;

import Exceptions.*;

/**
 *
 * @author Joao
 */
public class ArrayOrderedList<T> extends ArrayList<T> implements OrderedListADT<T>, Iterable<T> {

    /**
     * Método construtor sem tamamnho defenido para a lista
     */
    public ArrayOrderedList() {
        super();
    }

    /**
     * Método construtor com o tamanho para a lista definido através do
     * parametro
     *
     * @param arraySize tamanho da lista
     */
    public ArrayOrderedList(int arraySize) {
        super(arraySize);
    }

    /**
     * Método que irá adicionar se for comparável, um elemento do tipo T à lista
     * de forma a que a lista continue ordenada e a expanda caso seja necessário
     *
     * @param element elemento a adicionar
     */
    @Override
    public void add(T element) throws NotComparableException {
        if (!(element instanceof Comparable)) {
            throw new NotComparableException("O objeto que pretende adicionar não é comparável");
        }

        if (this.list.length == this.size()) {
            this.expandCapacity();
        }

        int position = 0;
        boolean found = false;
        Comparable<T> comparElement = (Comparable<T>) element;

        while (!found && position < this.rear) {
            if (comparElement.compareTo(this.list[position]) > 0) {
                position++;
            } else {
                found = true;
            }
        }

        for (int j = this.rear; j > position; j--) {
            this.list[j] = this.list[j - 1];
        }

        this.list[position] = element;
        this.rear++;
        this.modCount++;
    }

}
