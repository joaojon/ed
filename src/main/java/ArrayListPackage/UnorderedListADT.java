/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayListPackage;

import Exceptions.*;

/**
 *
 * @author Joao
 */
public interface UnorderedListADT<T> extends ListADT<T> {

    /**
     * Adds an element to the front of the list
     *
     * @param element the element to be added to this list
     */
    public void addToFront(T element);

    /**
     * Adds an element to the rear of the list
     *
     * @param element the element to be added to this list
     */
    public void addToRear(T element);

    /**
     * Adds an element after a particular element already in the list
     *
     * @param elementToAdd the element to be added to this list
     * @param elementBefore the element the before the one we want to add
     * @throws ElementNotFoundException if the element isn't found
     */
    public void addAfter(T elementToAdd, T elementBefore) throws ElementNotFoundException;
}
