package Exceptions;

/**
 *
 * @author Joao
 */
public class InvalidMapException extends Exception {

    /**
     * Creates a new instance of <code>InvalidMapException</code> without
     * detail message.
     */
    public InvalidMapException() {
    }

    /**
     * Constructs an instance of <code>InvalidMapException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidMapException(String msg) {
        super(msg);
    }
}
