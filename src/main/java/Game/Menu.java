package Game;

import Exceptions.InvalidMapException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.io.*;
import java.util.Scanner;

/***
 *
 * Class to showing menu, set map, difficulty and start game
 *
 * @author Joao Coimbra
 * @author Daniel Sousa
 */
public class Menu {
    /**
     * Variable to do not repeat every time want use divider
     */
    private static String DIVIDER = "========================================================";
    /**
     * Variable to do not repeat every time want use line
     */
    private static String LINE = "―――――――――――――――――――――――――";
    /**
     * Scanner to reuse on menus
     * This way it is not necessary to re-create when necessary
     */
    private Scanner scanner = new Scanner(System.in);
    /**
     * Variable boolean to stop the while loop when enter in valid switch case
     */
    private boolean validCase = false;

    /**
     * Class Game to start after fill all necessary data
     */
    private Game game;

    public static void main(String[] args) {
        try {
            new Menu();
        } catch (NullPointerException | IOException | ParseException error) {
            System.out.println(error.getMessage());
        }
    }

    /**
     * Method to user set the map
     *
     * @throws ParseException Parse error
     * @throws IOException    IO error
     */
    private Menu() throws IOException, ParseException {
        String choice = "";
        // Boolean to repeat menu if insert invalid map name
        boolean nameOfMap = false;
        boolean validPath = false;
        String mapName = "";
        int difficulty = 0;

        System.out.println("\tHello! Only one thing before play.");
        System.out.println(DIVIDER);

        while (!nameOfMap || !validPath) {
            System.out.println(getAllMaps());

            System.out.println("Insert the option: ");
            // Prevent bug fixes
            scanner = new Scanner(System.in);
            choice = scanner.nextLine();

            if (validateMap(choice) != null ) {
                nameOfMap = true;

                mapName = (String) validateMap(choice).get("fileName");

                String level = "";
                while (!level.matches("[1-3]")) {
                    System.out.println(LINE);
                    System.out.println("Insert the difficulty:");
                    System.out.println("1) Easy | 2) Medium | 3) Hard");
                    level = scanner.next();
                    System.out.println(LINE);

                    // REGEX to validate if value is numeric and between 1 and 3
                    if (!level.matches("[1-3]")) {
                        System.out.println("Please insert one value valid to difficulty!");
                    } else {
                        difficulty = Integer.parseInt(level);
                    }
                }

                try {
                    this.game = new Game(mapName, difficulty);
                    validPath = true;
                    start();
                } catch (InvalidMapException e) {
                    System.out.println(e.getMessage());
                }
            } else {
                // Not enter in if, show message
                System.out.println("Invalid map name!");
            }
        }
    }

    /**
     * Method to show menu after choosed map and difficulty
     */
    public void start() {

        // While using valid case variable
        while (!validCase) {
            System.out.println("\tWelcome! Ready to play?");
            System.out.println(DIVIDER);
            System.out.println("  " + LINE + LINE + LINE + LINE + LINE + LINE);
            System.out.println("|\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  1) Manual Mode  \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|");
            System.out.println("|\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  2) Simulation Mode\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|");
            System.out.println("|\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  3) View Leader board\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|");
            System.out.println("|\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  4) View Matrix\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|");
            System.out.println("|\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  5) View Map    \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|");
            System.out.println("|\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  6) Exit        \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|");
            System.out.println("  " + LINE + LINE + LINE + LINE + LINE + LINE + "\n");

            // Get option value
            System.out.println("Insert your option: ");

            // Option is type of String because use REGEX to validate
            String option = scanner.next();

            // Validate if the option have value between 1 and 9
            if (!option.matches("[1-9]")) {
                System.out.println("Please, insert only numbers!");
            } else {
                System.out.println("You choose the option number " + option + "\n");

                switch (Integer.parseInt(option)) {
                    case 1:
                        // Manual Mode
                        this.validCase = true;
                        manualMode();
                        returnToMenu();

                        break;

                    case 2:
                        // Simulation Mode
                        this.validCase = true;
                        this.game.simulation();
                        returnToMenu();

                        break;

                    case 3:
                        // View Leader Board
                        this.validCase = true;
                        readRatings();
                        returnToMenu();

                        break;

                    case 4:
                        // Print Matrix
                        this.validCase = true;
                        System.out.println(this.game.getMatrix());
                        returnToMenu();

                        break;

                    case 5:
                        // View Map
                        validCase = true;
                        this.game.displayMap();
                        returnToMenu();

                        break;

                    case 6:
                        // Exit
                        this.validCase = true;
                        System.out.println("Bye!");
                        System.exit(0);
                        break;

                    default:
                        System.out.println("Please insert one valid option!");
                        break;
                }
            }
        }
    }

    /**
     * Method to set name of player and start manual mode
     */
    private void manualMode() {
        Scanner scanner = new Scanner(System.in);
        String playerName = "";

        System.out.println("Welcome to Manual Mode!");
        System.out.println(LINE);

        System.out.println("Insert player name:");
        playerName = scanner.next();

        this.game.manual(playerName);
    }

    /**
     * Method to read ratings and display them
     */
    public void readRatings() {
        boolean found = false;
        String result = "";

        try {
            JSONParser parser = new JSONParser();

            // Object to get scores
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("Ratings/ratings.json"));
            JSONArray mapRatings = (JSONArray) jsonObject.get("maps");

            for (Object obj : mapRatings) {
                JSONObject mapRating = (JSONObject) obj;
                int level = ((Number) mapRating.get("level")).intValue();
                double initialHealth = ((Number) mapRating.get("initialHealth")).doubleValue();

                if (mapRating.get("mapName").equals(this.game.getName()) && this.game.getLevel() == level && this.game.getHealth() == initialHealth) {
                    found = true;
                    result += LINE + "\n";
                    result += "Map Name: " + mapRating.get("mapName") + "\n";
                    result += "Dificulty: " + mapRating.get("level") + "\n";
                    result += "Initial Health: " + mapRating.get("initialHealth") + "\n";
                    result += LINE + "\n";

                    JSONArray scores = (JSONArray) mapRating.get("scores");

                    for (Object scoresAvaible : scores) {
                        JSONObject score = (JSONObject) scoresAvaible;
                        result += "| Player " + score.get("player") + "\n";
                        result += "| Path: " + score.get("path") + "\n";
                        result += "| Health: " + score.get("health") + "\n";
                        result += LINE + "\n";
                    }
                }
            }
            if (!found) {
                System.out.println(LINE + "\n" + "There's no ratings for this map, this difficulty and this initial health.\n" + LINE);
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }

        System.out.println(result);
    }

    /**
     * Method to print option to return menu or not
     * This method was created to avoid repeating every time it was necessary to execute this option
     * <p>
     * Press Y to yes or N to exit
     */
    private void returnToMenu() {
        System.out.println("You want return to menu? Y to return or N to exit");
        String menu = scanner.next();

        switch (menu) {
            case "Y":
            case "y":
                this.validCase = false;
                break;

            case "N":
            case "n":
                System.out.println("Bye!");
                System.exit(0);
                break;

            default:
                System.out.println("Insert a valid option!");
                returnToMenu();
        }

    }

    /**
     * Method to get all names of maps in Map folder
     * This method is used on start to show all names of maps (element "nome" in JSON)
     *
     * @return String with maps names
     * @throws IOException    IO error
     * @throws ParseException if have any error on parse
     */
    private String getAllMaps() throws IOException, ParseException {
        String result = "";
        JSONArray maps = this.getMapsArray();

        for (Object obj : maps) {
            JSONObject map = (JSONObject) obj;
            result += " > " + map.get("mapName") + "\n";
        }

        return result;
    }

    /**
     * Method to validate if the name is valid
     * Return null if the name not valid, or return JSONObject if the name is valid
     * JSONObject contains fileName (name of file JSON on Map folder) and mapName (element "nome" in JSON)
     *
     * @param name name of the map
     * @return NULL if the name isn't valid JSONObject otherwise
     * @throws IOException    IO error
     * @throws ParseException if have any error on parse
     */
    private JSONObject validateMap(String name) throws IOException, ParseException {
        JSONArray maps = this.getMapsArray();

        for (Object obj : maps) {
            JSONObject map = (JSONObject) obj;
            if (name.equals(map.get("mapName"))) {
                return map;
            }
        }

        return null;
    }

    /**
     * Method to get a JSONArray with all maps in directory Map
     * This JSONArray contains JSON Object with fileName (name of file in Map folder) and
     * mapName (element "nome" in JSON file)
     *
     * @return Json Array with all maps in directory Map
     * @throws IOException    IO error
     * @throws ParseException if was any error on parse
     */
    private JSONArray getMapsArray() throws IOException, ParseException {
        JSONArray arrayWithBothNames = new JSONArray();

        File[] listOfFiles = new File("./Map").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (name.toLowerCase().endsWith(".json")) {
                    return true;
                } else {
                    return false;
                }
            }
        });

        for (File fileName : listOfFiles) {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("Map/" + fileName.getName()));

            JSONObject bothname = new JSONObject();

            String[] mapNameSplited = fileName.getName().split(".json");
            bothname.put("fileName", mapNameSplited[0]);
            bothname.put("mapName", jsonObject.get("nome"));

            arrayWithBothNames.add(bothname);
        }

        return arrayWithBothNames;
    }
}
