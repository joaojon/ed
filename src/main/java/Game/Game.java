package Game;

import ArrayListPackage.ArrayUnorderedList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.lang.Number;
import java.util.Iterator;
import java.util.Scanner;

import Exceptions.*;


import ArrayListPackage.ArrayUnorderedList;

import static ConfigReader.ConfigReader.getNumberOfRooms;

/**
 * Class Game that has the principal implementations of the methods needed to a correct functionality of the game, this
 * includes the necessary variables as the graph that represents the map, the name, the initial health and the level
 *
 * @author Joao Coimbra
 * @author Daniel Sousa
 */
public class Game {
    /**
     * Map of the game that is an oriented network
     */
    private GameDirectedGraph<String> map;
    /**
     * Name of the game
     */
    private String name;
    /**
     * Initial health
     */
    private double health;
    /**
     * Difficulty of the actual game
     */
    private int level;
    /**
     * default denomination of the exit on the maps
     */
    private static final String DEFAULTEXIT = "exterior";
    /**
     * deafult denomination of the entry on the maps
     */
    private static final String DEFAULTENTRY = "entrada";

    /**
     * Variable to do not repeat every time want use divider
     */
    private static final String DIVIDER = "========================================================";

    /**
     * Variable to do not repeat every time want use line
     */
    private static final String LINE = "―――――――――――――――――――――――――";

    /**
     * Constructor method for a game
     *
     * @param mapName Map name
     * @param level   level of the actual game
     */
    public Game(String mapName, int level) throws InvalidMapException {
        this.map = new GameDirectedGraph<String>(getNumberOfRooms(mapName));
        this.level = level;
        this.readMap(mapName);
        if (!this.map.hasPathBetween(DEFAULTENTRY, DEFAULTEXIT)) {
            throw new InvalidMapException("There's no path between the entry and the exit.\nChoose another map...");
        }
        if (this.map.shortestPathWeight(DEFAULTENTRY, DEFAULTEXIT) >= 100) {
            throw new InvalidMapException("It's impossible to reach the exit with life using the giving map, and the giving level.\nChoose another map...");
        }
    }

    /**
     * Method that returns a string with the representation of both matrix's (adj and weights)
     *
     * @return a string with the representation of both matrix's (adj and weights)
     */
    public String getMatrix() {
        return this.map.toString();
    }

    public double getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    /**
     * Verifies if a vertex is in the network, if not adds it to the network
     *
     * @param vertex vertex to verify and add if needed
     */
    private void createVertex(String vertex) {
        try {
            this.map.getIndex(vertex);
        } catch (ElementNotFoundException e) {
            this.map.addVertex(vertex);
        }
    }

    /**
     * Generates a directed netowrk with the correct connections by reading a json
     *
     * @param mapName Name of the json file tha has the map
     */
    private void generateNetwork(String mapName) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("Map/" + mapName + ".json"));
            JSONArray mapArray = (JSONArray) jsonObject.get("mapa");

            for (Object object : mapArray) {
                JSONObject roomInfo = (JSONObject) object;
                String roomName = (String) roomInfo.get("aposento");
                double ghost = ((Number) roomInfo.get("fantasma")).doubleValue();

                this.createVertex(roomName);

                JSONArray roomConnectionsArray = (JSONArray) roomInfo.get("ligacoes");
                for (Object o : roomConnectionsArray) {
                    String connectionName = (String) o;

                    this.createVertex(connectionName);

                    this.updateEdges(roomName, connectionName, ghost);
                }
            }
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * For a given room and a given connection updates the edges values correctly
     *
     * @param roomName        actual room
     * @param connectionName  room connection
     * @param roomGhostWeight room ghost weight
     */
    private void updateEdges(String roomName, String connectionName, double roomGhostWeight) {
        if (!this.map.areConnected(roomName, connectionName)) {
            this.map.addEdgeInBothWays(roomName, connectionName);
        }
        if (roomGhostWeight > 0) {
            this.map.addEdge(connectionName, roomName, (roomGhostWeight * level));
        }
    }

    /**
     * Method that reads a given map and creates a directed network with it
     *
     * @param mapName Name of the json file tha has the map
     */
    private void readMap(String mapName) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("Map/" + mapName + ".json"));
            this.name = (String) jsonObject.get("nome");
            this.health = ((Number) jsonObject.get("pontos")).doubleValue();

            this.generateNetwork(mapName);
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Returns the health left
     *
     * @param damage the actual cost of the path
     * @return the health points left
     */
    private double getHealthLeft(double damage) {
        return this.health - damage;
    }

    /**
     * Method that does the simulation mode for the actual, fives the shortest path and the points of health remaining
     * using the respective path
     */
    public void simulation() {
        Iterator shortestPathIterator = this.map.iteratorShortestPath(DEFAULTENTRY, DEFAULTEXIT);
        double shortestPathWeight = this.map.shortestPathWeight(DEFAULTENTRY, DEFAULTEXIT);

        System.out.println(LINE);
        System.out.println("Simulation Mode");
        System.out.println(LINE);
        System.out.print("| The best track to this map is : ");
        while (shortestPathIterator.hasNext()) {
            System.out.print((String) shortestPathIterator.next());
            if (shortestPathIterator.hasNext()) {
                System.out.print(" -> ");
            }
        }
        System.out.println("\n| Using this path you reach the outside with " + getHealthLeft(shortestPathWeight) + " points of health left.");
        System.out.println(LINE);
    }

    /**
     * Method that gives the player the change of choose his path by a list of every adjency room of the current room
     * as an option to go further.This mod end's when the player reaches "exterior" or get's out of point's of health.
     * In this mod and by using an auxiliary method, the record of the play is registered in a specific json just for
     * ratings.
     *
     * @param playerName name of the player
     */
    public void manual(String playerName) {
        double currentHealth = this.health;
        String currentVertex = DEFAULTENTRY;
        Scanner scanner = new Scanner(System.in);
        ArrayUnorderedList<String> path = new ArrayUnorderedList<>();
        path.addToRear(currentVertex);

        while (currentHealth > 0 && !currentVertex.equals(DEFAULTEXIT)) {
            System.out.println(LINE);
            System.out.println("| Current location: " + currentVertex + " |");
            System.out.println("| Current health: " + currentHealth + "|");
            System.out.println(LINE);
            System.out.println("Go to: ");
            Iterator itrAdjVertexs = this.map.getAdjVertexs(currentVertex);

            ArrayUnorderedList<String> tempList = new ArrayUnorderedList<String>();

            while (itrAdjVertexs.hasNext()) {
                String connection = (String) itrAdjVertexs.next();
                if (!connection.equals(DEFAULTENTRY)) {
                    tempList.addToRear(connection);
                    System.out.println("- " + connection);
                }
            }

            System.out.println(LINE);
            System.out.println("Chose one of the giving rooms:");
            String goTo = scanner.nextLine();

            try {
                if (!tempList.contains(goTo)) {
                    System.out.println("*** Chose a valid room ***");
                } else {
                    path.addToRear(goTo);
                    currentHealth -= this.map.getEdgeWeigth(currentVertex, goTo);
                    currentVertex = goTo;
                }
            } catch (EmptyCollectionException e) {
                System.out.println(e.getMessage());
            }
        }

        if (currentHealth > 0) {
            System.out.println(DIVIDER);
            System.out.println("Victory");
            System.out.print("\nPoints of health left: " + currentHealth);

        } else {
            System.out.println("Buhhhhhhh you are a looser...");
            currentHealth = 0;
        }
        System.out.print("\nPath: ");
        Iterator pathIterator = path.iterator();
        this.writeRatingJSON(playerName, path.iterator(), currentHealth);

        while (pathIterator.hasNext()) {
            System.out.print(pathIterator.next());
            if (pathIterator.hasNext()) {
                System.out.print(" -> ");
            }
        }
        System.out.println("\n" + DIVIDER);
    }

    /**
     * Given an player name, the path of the move(the final path) and the health left, this method is responsible to
     * write the details of the entire manual game plays, assigning each play to a group of similar plays (ex: plays
     * int the same map, same difficulty and same initial points of health. This method registers the ratings in a json
     * called ratings.json in the directory Ratings
     *
     * @param playerName name of the player
     * @param path       final path of the game mode
     * @param health     health left (0 if defeated)
     */
    private void writeRatingJSON(String playerName, Iterator path, Double health) {
        String ratingsPath = "Ratings/ratings.json";
        File file = new File(ratingsPath);
        String playerPath = "";

        while (path.hasNext()) {
            playerPath += path.next();
            if (path.hasNext()) {
                playerPath += " -> ";
            }
        }

        //create an object with the new score
        JSONObject insertScore = new JSONObject();
        insertScore.put("player", playerName);
        insertScore.put("health", health);
        insertScore.put("path", playerPath);
        JSONObject map = new JSONObject();
        JSONArray scores = new JSONArray();

        //create the map element with the score used only when this map, in this difficulty and this initial health
        // doesn't have previous ratings or there's no json ratings file
        scores.add(insertScore);
        map.put("mapName", this.name);
        map.put("level", this.level);
        map.put("initialHealth", this.health);
        map.put("scores", scores);

        // if the file doesn't exist
        if (!file.exists()) {
            try (FileWriter write = new FileWriter(ratingsPath)) {
                JSONObject ratings = new JSONObject();
                JSONArray maps = new JSONArray();

                maps.add(map);
                ratings.put("maps", maps);

                write.write(ratings.toJSONString());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            try {
                JSONParser parser = new JSONParser();
                JSONObject ratings = (JSONObject) parser.parse(new FileReader(ratingsPath));
                JSONArray maps = (JSONArray) ratings.get("maps");
                boolean found = false;

                for (Object obj : maps) {
                    JSONObject mapRating = (JSONObject) obj;
                    int level = ((Number) mapRating.get("level")).intValue();

                    // search for the element that has the same name, same difficulty and same initial health as the one
                    // that we want to add
                    if (mapRating.get("mapName").equals(this.name) && mapRating.get("initialHealth").equals(this.health) && level == this.level) {
                        found = true;
                        JSONArray scoresArray = (JSONArray) mapRating.get("scores");
                        JSONArray scoresOrdered = new JSONArray();
                        boolean newScoreAdded = false;

                        // build a new array with the new score and order it, so that we can replace the old one
                        for (Object obj2 : scoresArray) {
                            JSONObject compareScore = (JSONObject) obj2;

                            if (!newScoreAdded && (((Number) compareScore.get("health")).doubleValue() < ((Number) insertScore.get("health")).doubleValue())) {
                                newScoreAdded = true;
                                scoresOrdered.add(insertScore);
                            }
                            scoresOrdered.add(compareScore);
                        }
                        //worst score scenario
                        if (!newScoreAdded) {
                            scoresOrdered.add(insertScore);
                        }
                        mapRating.remove("scores");
                        mapRating.put("scores", scoresOrdered);
                    }
                }
                //if there's no ratings  for this map, difficulty and initial health
                if (!found) {
                    maps.add(map);
                }
                //write the json object updated in the file
                try (FileWriter write = new FileWriter(ratingsPath)) {
                    write.write(ratings.toJSONString());
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            } catch (IOException | ParseException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Method that displays the map information. The rooms, the connections and the information of a ghost in each room
     * of a current map
     */
    public void displayMap() {
        System.out.println(DIVIDER);
        System.out.println("\t\t\t\t\tMap view");
        System.out.println(DIVIDER);

        try {
            Iterator<String> bfsItr = this.map.iteratorBFS(DEFAULTENTRY);
            while (bfsItr.hasNext()) {
                String current = bfsItr.next();
                System.out.println("The room '" + current + "' has connections to:");

                Iterator<String> connectitonsItr = this.map.getAdjVertexs(current);

                double ghost = 0;

                while (connectitonsItr.hasNext()) {
                    String adjRoom = connectitonsItr.next();
                    System.out.println("- '" + adjRoom + "'");
                    ghost = this.map.getEdgeWeigth(adjRoom, current);
                }

                if (ghost > 0) {
                    System.out.println("Careful, '" + current + "' has a ghost that causes " + ghost + " points of damage!");
                } else {
                    System.out.println("Here in '" + current + "' you are safe, there's no ghost.");
                }

                System.out.println(LINE);
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
