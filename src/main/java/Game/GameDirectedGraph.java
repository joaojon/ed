package Game;

import ArrayListPackage.ArrayUnorderedList;
import Graph.DirectedNetwork;
import Exceptions.*;
import QueuePackage.LinkedQueue;

import java.util.Iterator;

/***
 * Class that extends Directed Network with specific methods for a game
 *
 * @author Joao Coimbra
 * @author Daniel Sousa
 */
public class GameDirectedGraph<T> extends DirectedNetwork<T> {

    /**
     * Constructor method  for a game directed graph with defined capacity
     *
     * @param numberOfRooms capacity of the graph
     */
    public GameDirectedGraph(int numberOfRooms) {
        super(numberOfRooms);
    }

    /**
     * Adds edge's between 2 vertexes with no weight,(weight == 0) in both directions
     *
     * @param vertex1 vertex 1
     * @param vertex2 vertex 2
     */
    protected void addEdgeInBothWays(T vertex1, T vertex2) {
        this.addEdge(vertex1, vertex2, 0);
        this.addEdge(vertex2, vertex1, 0);
    }

    /**
     * Given 2 vertex, returns true if they are connected in both ways
     *
     * @param vertex1 vertex 1
     * @param vertex2 vertex 2
     * @return true if they are connected in both ways, false otherwise
     */
    protected boolean areConnected(T vertex1, T vertex2) {
        try {
            int index1 = this.getIndex(vertex1);
            int index2 = this.getIndex(vertex2);

            if (this.indexIsValid(index1) && this.indexIsValid(index2) && this.adjMatrix[index1][index2] && this.adjMatrix[index2][index1]) {
                return true;
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    /**
     * Given a vertex index, an iterator  with the adjency vertex's indices is returned
     *
     * @param vertexIndex actual vertex index
     * @return an iterator with the adjency vertex's indices to the given vertex
     */
    protected Iterator<Integer> getAdjVertexsIndices(int vertexIndex) {
        ArrayUnorderedList<Integer> adjVertexs = new ArrayUnorderedList<Integer>();

        for (int i = 0; i < this.numVertices; i++) {
            if (this.adjMatrix[vertexIndex][i]) {
                adjVertexs.addToRear(i);
            }
        }

        return adjVertexs.iterator();
    }

    /**
     * Given a vertex, an iterator with the adjency vertex's is returned
     *
     * @param vertex actual vertex
     * @return an iterator with the adjency vertex's to the given vertex
     */
    public Iterator<T> getAdjVertexs(T vertex) {
        ArrayUnorderedList<T> templist = new ArrayUnorderedList();

        try {
            int vertexIndex = this.getIndex(vertex);

            if (!indexIsValid(vertexIndex)) {
                return templist.iterator();
            }

            Iterator<Integer> it = this.getAdjVertexsIndices(vertexIndex);

            while (it.hasNext()) {
                templist.addToRear(this.vertices[(it.next())]);
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return templist.iterator();
    }

    /**
     * Validate if there's at least on possible path between 2 given vertexes
     *
     * @param startRoom start room
     * @param targetRoom target vertex
     *
     * @return true if there's at least one path between 2 given vertexes
     */
    protected boolean hasPathBetween(T startRoom,T targetRoom){
        boolean hasPathBetween = false;

        try {
            Iterator itrBFS = this.iteratorBFS(startRoom);

            while(itrBFS.hasNext()){
                if(itrBFS.next().equals(targetRoom)){
                    hasPathBetween = true;
                }
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return hasPathBetween;
    }
}
