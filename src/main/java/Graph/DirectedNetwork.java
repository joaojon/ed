package Graph;

import ArrayListPackage.ArrayUnorderedList;
import LinkedHeapPackage.LinkedHeap;

import java.util.Iterator;

import Exceptions.*;

/**
 * Directed Network Class
 *
 * @author Joao Coimbra
 * @author Daniel Sousa
 */
public class DirectedNetwork<T> extends DirectedGraph<T> implements NetworkADT<T> {
    /**
     * Variable to insert divider (not repeat every time)
     */
    private static final String DIVIDER = "========================================================\n";
    /**
     * Matrix with the corresponding weigtht
     */
    private double[][] weightMatrix;
    /**
     * String returned when the graph is empty (not repeat every time)
     */
    private static final String EMPTY = "Graph is empty";

    /**
     * Constructor method for a directed network with a capacity given by a
     * parameter
     *
     * @param capacity inicial capacity of the network
     */
    public DirectedNetwork(int capacity) {
        super(capacity);
        this.weightMatrix = new double[capacity][capacity];
        for (int i = 0; i < capacity; i++) {
            for (int j = 0; j < capacity; j++) {
                this.weightMatrix[i][j] = Double.POSITIVE_INFINITY;
            }
        }
    }

    /**
     * Inserts an edge between two vertices of the graph from v1 to v2 with a
     * specific weight
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @param weight  the weight of the edge
     */
    @Override
    public void addEdge(T vertex1, T vertex2, double weight) {
        super.addEdge(vertex1, vertex2);

        setEdgeWeight(vertex1, vertex2, weight);
    }

    /**
     * Method that returns the shortest path weigth between two vertices if possible -1 if not
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @return the shortest path weigth between two vertices if possible, -1 if not
     */
    @Override
    public double shortestPathWeight(T vertex1, T vertex2) {
        double weight = 0;

        try {
            int startIndex = this.getIndex(vertex1);
            int targetIndex = this.getIndex(vertex2);

            if (!this.indexIsValid(startIndex) || !this.indexIsValid(targetIndex)) {
                return -1;
            }

            Iterator shortestPathIterator = this.iteratorShortestPathIndices(startIndex, targetIndex);

            int index1;
            int index2;

            if (shortestPathIterator.hasNext()) {
                index1 = (int) shortestPathIterator.next();
            } else {
                return -1;
            }

            while (shortestPathIterator.hasNext()) {
                index2 = (int) shortestPathIterator.next();
                weight += this.weightMatrix[index1][index2];
                index1 = index2;
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return weight;
    }

    /**
     * Method to define the weight at the weight matrix of the class
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @param weight  the weight of the edge
     */
    private void setEdgeWeight(T vertex1, T vertex2, double weight) {
        if (weight < 0) {
            throw new IllegalArgumentException("The weight must be mor or equal to 0");
        }

        try {
            int index1 = super.getIndex(vertex1);
            int index2 = super.getIndex(vertex2);

            if (super.indexIsValid(index1) && super.indexIsValid(index2)) {
                this.weightMatrix[index1][index2] = weight;
            }
        } catch (ElementNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Given the start vertex index and the target index, returns an iterator with the shortest path, if possible
     *
     * @param startIndex  start vertex index
     * @param targetIndex target vertex index
     * @return an iterator that contains the indices of the vertices that are in the shortest path between the two
     * given vertices.
     */
    private Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) {
        double[] pathWeightList = new double[this.numVertices];
        boolean[] visitedList = new boolean[this.numVertices];

        // initialize the path weight with all values with infinity value and visited with false
        for (int i = 0; i < this.numVertices; i++) {
            pathWeightList[i] = Double.POSITIVE_INFINITY;
            visitedList[i] = false;
        }

        LinkedHeap<Double> pathMinHeap = new LinkedHeap<>();
        int[] predecessorList = new int[this.numVertices];

        // initialize all the variables in order to begin in the startIndex
        pathWeightList[startIndex] = 0;
        predecessorList[startIndex] = -1;
        visitedList[startIndex] = true;

        // Update the pathWeight for each vertex except the startVertex, once we already treat that special case already
        // All vertices not adjacent to the startVertex have a pathWeight of infinity for now.
        for (int i = 0; i < this.numVertices; i++) {
            if (!visitedList[i]) {
                pathWeightList[i] = pathWeightList[startIndex] + this.weightMatrix[startIndex][i];
                predecessorList[i] = startIndex;
                pathMinHeap.addElement(pathWeightList[i]);
            }
        }

        int currentIndex;
        double weight = 0;
        ArrayUnorderedList<Integer> shortestPath = new ArrayUnorderedList<>();

        do {
            try {
                weight = pathMinHeap.removeMin();
            } catch (EmptyCollectionException e) {
                System.out.println(e.getMessage());
            }
            pathMinHeap = new LinkedHeap<>();
            // When there's no possible path
            if (weight == Double.POSITIVE_INFINITY)
                return shortestPath.iterator();
            else {
                currentIndex = getIndexOfAdjVertex(visitedList, pathWeightList, weight);
                visitedList[currentIndex] = true;
            }

            // Update the pathWeight for each vertex that has not been visited yet and is adjacent to the last vertex
            // that was visited. Also, add each unvisited vertex to the heap.
            for (int i = 0; i < this.numVertices; i++) {
                if (!visitedList[i]) {
                    if ((this.weightMatrix[currentIndex][i] < Double.POSITIVE_INFINITY) && (pathWeightList[currentIndex] + this.weightMatrix[currentIndex][i]) < pathWeightList[i]) {
                        pathWeightList[i] = pathWeightList[currentIndex] + this.weightMatrix[currentIndex][i];
                        predecessorList[i] = currentIndex;
                    }
                    pathMinHeap.addElement(pathWeightList[i]);
                }
            }
        } while (!pathMinHeap.isEmpty() && !visitedList[targetIndex]);

        // add to a the result list  the indexes of the shortest path
        currentIndex = targetIndex;
        shortestPath.addToFront(currentIndex);
        do {
            currentIndex = predecessorList[currentIndex];
            shortestPath.addToFront(currentIndex);
        } while (currentIndex != startIndex);

        return shortestPath.iterator();
    }


    /**
     * Method that returns the index of the the vertex that is adjacent to the vertex with the given index and also has a
     * pathWeight equal to weight.
     *
     * @param visited    array that indicates if the vertices have already been visited
     * @param pathWeight array with the weights of each vertices
     * @param weight     the cost that we want to find
     * @return the index of the the vertex that is adjacent to the vertex with the given index and also has a
     * pathWeight equal to weight.
     */
    private int getIndexOfAdjVertex(boolean[] visited, double[] pathWeight, double weight) {
        for (int i = 0; i < this.numVertices; i++)
            if ((pathWeight[i] == weight) && !visited[i])
                for (int j = 0; j < this.numVertices; j++)
                    if (this.weightMatrix[j][i] < Double.POSITIVE_INFINITY && visited[j])
                        return i;

        return -1;  // Unexpected return
    }

    /**
     * Returns an iterator that contains the shortest path between the two
     * vertices.
     *
     * @param startVertex  the starting vertex
     * @param targetVertex the ending vertex
     * @return an iterator that contains the shortest path between the two
     * vertices
     */
    public Iterator<T> iteratorShortestPath(T startVertex, T targetVertex) {
        ArrayUnorderedList templist = new ArrayUnorderedList();

        try {
            int startIndex = getIndex(startVertex);
            int targetIndex = getIndex(targetVertex);

            if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) || (startIndex == targetIndex) || isEmpty()) {
                return templist.iterator();
            }

            Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);

            while (it.hasNext()) {
                templist.addToRear(this.vertices[(it.next())]);
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return templist.iterator();
    }

    /**
     * Method that returns the weight from one vertex to another if there's connection from vertex1 to vertex2
     *
     * @param vertex1 first vertex
     * @param vertex2 second vertex
     * @return the weight of an edge between 2 given edges
     */
    public double getEdgeWeigth(T vertex1, T vertex2) {
        double result = -1;

        try {
            int index1 = this.getIndex(vertex1);
            int index2 = this.getIndex(vertex2);

            if (this.adjMatrix[index1][index2]) {
                result = this.weightMatrix[index1][index2];
            } else {
                throw new IllegalArgumentException("Não existe ligação entre os vértices");
            }
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    /**
     * Returns a String representation of the adjency matrix, the weigth matrix of the network
     *
     * @return String representation of the adjency matrix, the weigth matrix of the network
     */
    public String toString() {

        if (this.numVertices == 0) {
            return EMPTY;
        }
        String result = "";

        result += getAdjacencyMatrix();

        result += "\n";

        result += getWeightMatrix();

        return result;
    }

    private String getWeightMatrix() {
        String result = "";

        if (this.numVertices == 0) {
            return EMPTY;
        }

        result += DIVIDER;
        result += "Weigth Matrix\n";
        result += DIVIDER;
        result += "index\t";

        for (int i = 0; i < this.numVertices; i++) {
            result += "" + i;
            if (i < 10) {
                result += "    ";
            }
        }
        result += "\n" + DIVIDER;

        for (int i = 0; i < this.numVertices; i++) {
            result += "" + i + "\t\t";

            for (int j = 0; j < this.numVertices; j++) {
                if (this.weightMatrix[i][j] == Double.POSITIVE_INFINITY) {
                    result += "--   ";
                } else {
                    result += this.weightMatrix[i][j] + " ";
                }
            }
            result += "\n";
        }

        return result;
    }

    private String getAdjacencyMatrix() {
        String result = "";

        if (this.numVertices == 0) {
            return EMPTY;
        }

        result += DIVIDER;
        result += "               Adjacency Matrix                \n";
        result += DIVIDER;
        result += "Index\t";

        for (int i = 0; i < this.numVertices; i++) {
            result += "" + i + " | ";
        }

        result += "Room\n" + DIVIDER;

        for (int i = 0; i < this.numVertices; i++) {
            result += "" + i + "\t\t";

            for (int j = 0; j < this.numVertices; j++) {
                if (this.adjMatrix[i][j]) {
                    result += "1" + " | ";
                } else {
                    result += "0" + " | ";
                }
            }

            result += this.vertices[i].toString() + "\n";
        }
        result += DIVIDER;
        result += "index\troom\n";
        result += DIVIDER;

        for (int i = 0; i < this.numVertices; i++) {
            result += "  " + i + "\t\t";
            result += this.vertices[i].toString() + "\n";
        }

        return result;
    }
}
