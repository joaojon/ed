/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import Exceptions.*;

/**
 * @author Joao
 */
public class DirectedGraph<T> extends Graph<T> implements GraphADT<T> {
    /**
     * Constructor of a Directed Graph with a default capacity of 10
     */
    public DirectedGraph() {
        super();
    }

    /**
     * Constructor of a Directed Graph with a capacity given by the user
     *
     * @param capacity inicial capacity of the graph
     */
    public DirectedGraph(int capacity) {
        super(capacity);
    }

    /**
     * Inserts an edge between two vertices of the graph from v1 to v2
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     */
    @Override
    public void addEdge(T vertex1, T vertex2) {
        try {
            addEdge(getIndex(vertex1), getIndex(vertex2));
        } catch (ElementNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Inserts an edge between two vertices of the graph from the vertex with
     * the index 1 to the one with the index 2
     *
     * @param index1 the first index
     * @param index2 the second index
     */
    private void addEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            this.adjMatrix[index1][index2] = true;
        }
    }
}
