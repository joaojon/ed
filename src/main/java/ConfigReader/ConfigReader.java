package ConfigReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

/**
 * This class is used to read JSON file with map and return some details about that map
 */
public class ConfigReader {
    /**
     * Method to calculate the number of rooms including the "entrada" and the "exterior", of a map,
     * in order to create a graph with the correct size
     *
     * @param mapName name of the json we want to read the map from
     * @return the number of rooms including the "entrada" and the "exterior"
     */
    public static int getNumberOfRooms(String mapName) {
        int numberOfRooms = 0;

        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("Map/" + mapName + ".json"));
            JSONArray mapArray = (JSONArray) jsonObject.get("mapa");

            // + 2 because of unlisted rooms entrada and exterior
            numberOfRooms = mapArray.size() + 2;
        } catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }

        return numberOfRooms;
    }
}
