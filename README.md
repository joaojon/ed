# Projeto de Estrutura de Dados

### **Objetivo:** Criar um jogo que dado um mapa de uma casa com vários aposentos (podendo os mesmos ter fantasmas que nos retiram pontos de vida caso entremos nos mesmos), tenha como objetivo o jogador chegar da entrada ao exterior com o maior número de pontos de vida. Este jogo deve ser implementado recorrendo a estruturas de dados implementadas por nós.

### **Ferramentas utilizadas:**
* GitLab

 <a href="#"><img src="https://static.concrete.com.br/uploads/2018/02/logo.png" width="100px" alt="Postman"></a>

* Intellij

<a href="#"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/IntelliJ_IDEA_Logo.svg/1200px-IntelliJ_IDEA_Logo.svg.png" width="100px" alt="Postman"></a>

* JAVA

<a href="#"><img src="https://img.ibxk.com.br/2013/10/16/16170722149.jpg" width="100px" alt="Postman"></a>

* SonarLint

<a href="#"><img src="https://miro.medium.com/max/400/0*YwfKKIhttnBi991F." width="100px" alt="Postman"></a>

> ### Notas Importantes:
> * A diretoria **Map** tem como propósito guardar todos os mapas que pretendemos que sejam possíveis de carregar para serem jogados;
> * A diretoria **Ratings** possui um ficheiro json que contém todos os registos de todas as jogadas efetuadas no modo manual do sistema, respetivamente separada;
> * A diretoria **javadoc** serve para a respetiva alocação da informação gerada no momento em que o mesmo é gerado.